// import { StatBlock } from "./StatBlock";
// import { Clonable, DataPoints, getMod, getXpMax, isEmpty, Journal, RangeValues, Tools } from "./Tools";

// export type EntityProps = {
//     name: string,
//     hp: RangeValues,
//     mp: RangeValues,
//     hitDie: number,
//     hpRollLog: Journal,
//     level: number,
//     attack: number,
//     speed: number,
//     statBlock: StatBlock,
//     xp: RangeValues,
// };

// // Will eventually be abstract, so enjoy this while you can!
// export class Entity<T extends EntityProps> implements Clonable<Entity<T>>, EntityProps {
//     name: string;
//     hp: RangeValues;
//     mp: RangeValues;
//     hitDie: number;
//     hpRollLog: Journal;
//     level: number;
//     attack: number;
//     speed: number;
//     statBlock: StatBlock;
//     xp: RangeValues;

//     constructor(state: Partial<EntityProps>) {
//         this.name = state.name || "Unknown";
//         this.hp = state.hp || { cur: 0, max: 0 }
//         this.mp = state.mp || { cur: 5, max: 7 }
//         this.hitDie = state.hitDie || 8;
//         this.level = state.level || 1;
//         this.attack = state.attack || 1;
//         this.speed = state.speed || 30;
//         this.xp = state.xp || { cur: 0, max: 1000 }

//         this.hpRollLog = state.hpRollLog || {} as Journal;

//         if (state.statBlock) {
//             this.statBlock = state.statBlock.clone();
//         } else {
//             this.statBlock = new StatBlock(Object.keys(Tools.base_stats));
//         }

//         this.update();
//     }

//     update(): void {
//         // console.log('entity::update', 'start', this.hp.max);
//         const L = this.hpRollLog;
//         const S = this.statBlock.stats;

//         // HP
//         if ((this.level > 0) && ((this.hp.max === 0) || isEmpty(this.hpRollLog))) {
//             let roll;
//             // console.log('Needs init', this);
//             if (this.level > 0) {
//                 roll = this.hitDie;
//                 this.hp.max = roll + Math.max(0, getMod(S['con']));
//                 L['level-1'] = roll;
//                 // this.hp
//             }

//             this.hp.cur = this.hp.max;
//         } else {
//             // Make HP max reflect stats
//             let hpm = this.hpRollLog['level-1'] + Math.max(0, getMod(S['con']));
//             hpm += Object.values(L).slice(1).map((e) => (Math.max(1, e + getMod(S['con'])))).reduce((a, e) => (a + e), 0);
//             this.hp.max = hpm; //Object.values(L).map((e) => (Math.max(1, e + getMod(S['con'])))).reduce((a, e) => (a + e));
//             this.hp.cur = Math.min(this.hp.max, this.hp.cur);
//         }
//         // XP
//         if (this.level > 0) {
//             this.xp.max = getXpMax(this.level);
//         }
//         // console.log('entity::update', 'end', this.hp.max);
//     }

//     clone(): Entity<T> {
//         const result = new Entity({
//             name: this.name,
//             hp: { cur: this.hp.cur, max: this.hp.max },
//             mp: { cur: this.mp.cur, max: this.mp.max },
//             level: this.level,
//             hitDie: this.hitDie,
//             hpRollLog: { ...this.hpRollLog } as Journal,
//             attack: this.attack,
//             statBlock: this.statBlock.clone(),
//             xp: { cur: this.xp.cur, max: this.xp.max },
//         });
//         return result;
//     }

//     getHpJournal(): Journal {
//         const result = {} as Journal;

//         this.getHpRolls().forEach((e, i) => (result[i] = e));

//         return result;
//     }

//     getHpRolls(): number[] {
//         return Object.values(this.hpRollLog).map((e, i) => {
//             if (i === 0) {
//                 return e + Math.max(0, getMod(this.statBlock.stats['con']));
//             } else {
//                 return Math.max(1, e + getMod(this.statBlock.stats['con']));
//             }
//         })
//     }

//     getDataPoint(dataPoint: DataPoints): { value: any, journal: Journal } {
//         const result: { value: any, journal: Journal } = { value: null, journal: {} as Journal };

//         switch (dataPoint) {
//             case DataPoints.Nothing:
//                 break;
//             case DataPoints.HP:
//                 result.value = this.hp.max;
//                 result.journal = this.getHpJournal();
//                 break;
//             case DataPoints.AC:
//                 let ac = 10;
//                 result.journal['Base'] = 10;
//                 // TODO: Limit dex mod based on armor max dex mod
//                 const dex_mod = getMod(this.statBlock.stats['dex']);
//                 ac += dex_mod;
//                 result.journal['Dex mod'] = dex_mod;
//                 // TODO: Components will affect this
//                 // TODO: Gear will affect this too
//                 result.value = Math.max(0, ac);
//                 break;
//             default:
//                 console.log(`Unhandled data point: ${DataPoints[dataPoint]} (${dataPoint})`);
//         }
//         return result;
//     }
// };

export default {};