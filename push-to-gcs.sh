#!/bin/bash

rm -rf build/ build*
npm install
npm run build-gcs

/usr/bin/rsync -vr --delete build/* react.theqcp.org:/var/www/html/dive-click/
