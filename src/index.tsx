import React from 'react';
import ReactDOM from 'react-dom/client';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import { GameStateProvider } from './context/GameContext';
import WelcomePanel from './pages/WelcomePanel';
import GamePanel from './pages/GamePanel';
import CreatePanel from './pages/CreatePanel';

import { App } from './App';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <React.StrictMode>
    <BrowserRouter basename={process.env.PUBLIC_URL}>
      <GameStateProvider>
        <Routes>
          <Route path="/" element={<App />}>
            <Route index element={<WelcomePanel />} />
            <Route path="/create" element={<CreatePanel />} />
            <Route path="/play" element={<GamePanel />} />
          </Route>
        </Routes>
      </GameStateProvider>
    </BrowserRouter>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals(/*console.log*/);
