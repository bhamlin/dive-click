import { Col, Container, Row } from "react-bootstrap";
import { Link } from "react-router-dom";

import { useGlobalState } from "../context/GameContext";
import { NewVictimButton } from "../ui/components/NewVictimButton";
import { RC } from "../ui/UiTools";

import PlayerView from "../ui/views/PlayerView";
import VictimView from "../ui/views/VictimView";

const GamePanel = () => {
  const { state } = useGlobalState();

  if (!state.player) {
    return (
      <div>
        <span>Something has gone horribly, terribly wrong. </span>
        <Link to="/create">Create a character</Link>
        <span> or </span>
        <Link to="/">start over.</Link>
      </div>
    );
  }

  return (
    <Container fluid>
      <Row>
        <Col xs={4}>
          <PlayerView actionsVisible={true} />
        </Col>
        <Col>
          <Container fluid>
            <RC>
              <NewVictimButton />
            </RC>
            <RC>
              <VictimView />
            </RC>
          </Container>
        </Col>
      </Row>
    </Container>
  )
};

export default GamePanel;