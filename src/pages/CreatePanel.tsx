import { useEffect } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { useGlobalState } from "../context/GameContext";

import PlayerStatBlockView from "../ui/views/PlayerStatBlockView";
import PlayerView from "../ui/views/PlayerView";

const CreatePanel = () => {
    const { state, setState } = useGlobalState();

    useEffect(() => {
        if (!state.alive) {
            setState({ ...state, alive: true });
        }
    });

    return (
        <Container fluid>
            <Row>
                <Col xs={4}>
                    <PlayerView editable={true} actionsVisible={false} miscVisible={false} />
                </Col>
                <Col>
                    <PlayerStatBlockView editable={true} />
                </Col>
            </Row>
        </Container>
    );
}

export default CreatePanel;