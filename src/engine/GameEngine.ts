import _ from "lodash";

import { DEFAULT_STATE, GameState, GameStateInterface } from "../context/GameStateInterface";
import { cloneEntity, Entity, getDefaultEntity } from "./Entity";
// import { CombatHandler } from "./handlers/CombatHandler";
import { EntityHandler } from "./handlers/EntityHandler";
import { DataPoints, doRoll, getEntityAttacks, getMod, Journal } from "./Tools";

export interface ActionHandler {
    handle(state: GameStateInterface, data?: any): GameStateInterface;
}

export class GameEngine {
    private static default_state: GameStateInterface;
    private static instance: GameEngine;

    public static getInstance(): GameEngine {
        if (!GameEngine.instance) {
            GameEngine.instance = new GameEngine();
        }

        return GameEngine.instance;
    }

    public static getDefaultState(): GameState {
        if (!GameEngine.default_state) {
            GameEngine.default_state = DEFAULT_STATE;
        }
        return GameEngine.default_state;
    }

    private handlers: {
        // combat: BaseActionHandler,
        entity: ActionHandler,
    };

    private constructor() {
        this.handlers = {
            // combat: new CombatHandler(),
            entity: new EntityHandler(),
        }
    }

    getCompleteState(state: GameState | undefined): GameState {
        return Object.assign(GameEngine.default_state, state || {});
    }

    getEntityDataPoint(dataPoint: DataPoints, entity: Entity | undefined): { value: any, journal: Journal } {
        if (entity) {
            // return entity.getDataPoint(dataPoint);
            //     getDataPoint(dataPoint: DataPoints): { value: any, journal: Journal } {
            const result: { value: any, journal: Journal } = { value: null, journal: {} as Journal };

            // console.log('enter', 'GameEngine::getEntityDataPoint', DataPoints[dataPoint], entity);

            const str_mod = getMod(entity.stats['str']);
            const dex_mod = getMod(entity.stats['dex']);
            // const con_mod = getMod(entity.stats['con']);
            // const int_mod = getMod(entity.stats['int']);
            // const wis_mod = getMod(entity.stats['wis']);
            // const cha_mod = getMod(entity.stats['cha']);

            switch (dataPoint) {
                case DataPoints.Nothing:
                    break;
                case DataPoints.HP:
                    // console.log('line', 'GameEngine::getEntityDataPoint', entity.hp);
                    result.value = entity.hp.max;
                    result.journal = this.getEntityHpJournal(entity);
                    break;
                case DataPoints.AC:
                    let ac = 10;
                    result.journal['Base'] = 10;
                    // TODO: Limit dex mod based on armor max dex mod
                    ac += dex_mod;
                    result.journal['Dex mod'] = dex_mod;
                    // TODO: Components will affect this
                    // TODO: Gear will affect this too
                    result.value = Math.max(0, ac);
                    break;
                case DataPoints.AttackBonus:
                    // let ab = getBaB(entity.level, entity.toHit);
                    let ab = getEntityAttacks(entity)[0];
                    result.journal['Base'] = ab;
                    result.journal['Str mod'] = str_mod;
                    result.value = ab;
                    break;
                default:
                    console.log(`Unhandled data point: ${DataPoints[dataPoint]} (${dataPoint})`);
            }

            // console.log('exit', 'GameEngine::getEntityDataPoint', result);

            return result;
            //     }
            // };
        } else {
            console.error('GameEngine::getEntityDataPoint called with undefined entity');
            return { value: null, journal: {} };
        }
    }

    getEntityHpJournal(entity: Entity): Journal {
        const result = {} as Journal;

        this.getEntityHpRolls(entity).forEach((e, i) => (result[i] = e));

        return result;
    }

    getEntityHpRolls(entity: Entity): number[] {
        return Object.values(entity.hpRollLog).map((e, i) => {
            if (i === 0) {
                return e + Math.max(0, getMod(entity.stats['con']));
            } else {
                return Math.max(1, e + getMod(entity.stats['con']));
            }
        })
    }

    getRandomEntity(): Entity {
        const e = getDefaultEntity();
        const names = [
            'Brurk', 'Bul', 'Zurk', 'Clokx', 'Zadvyrd', 'Glilvuic', 'Gneavleabs',
            'Ealziokt', 'Uvabs', 'Nienx', 'Qing', 'Chean', 'Iolt', 'Slaga',
            'Dikiesha', 'Qoplinga', 'Fivnizz', 'Khyrnash', 'Cearrash']

        e.name = _.sample(names) || 'Boblin';

        return e;
    }

    action(state: GameState, action: string, opts?: any): GameState {
        let stateUpdate = this.getCompleteState(state) as GameStateInterface;
        let actionGroup;

        if (action === 'attack') {
            actionGroup = 'attack';
        } else if (action === 'heal') {
            actionGroup = 'heal';
        } else if (action.match('modstat-.*')) {
            actionGroup = 'modstat';
        } else if (action.match('set-player-.*')) {
            actionGroup = 'set-player';
        } else {
            console.log(`Got action: ${action}`);
            console.log(`In group: ${actionGroup}`);
        }

        switch (actionGroup) {
            case 'attack':
                stateUpdate = this.basicAttackRound(stateUpdate);
                break;
            case 'heal':
                stateUpdate.player.hp.cur = stateUpdate.player.hp.max;
                stateUpdate = this.basicAttackRound(stateUpdate, { player: true });
                break;
            case 'set-player':
                this.handlers.entity.handle(stateUpdate, { actionGroup, action, opts });
                break;
            default:
                console.log(`Non-handled action group: ${actionGroup}`);
        }
        return stateUpdate;
    }

    basicAttackRound(state: GameStateInterface, skip: { player?: boolean, victim?: boolean } = { player: false, victim: false }) {
        if (state.victim && state.victim.alive) {
            // Initiative should probably be done with a list.
            // Will have to track player to pull it back out?
            // Not sure how to track others for more than two participants.
            // Maybe we just have 1v1 with player attacking first except
            //  in certain circumstances?
            let [player, victim] = [state.player, state.victim];
            if (!skip.player && player.alive) {
                console.log('Player attacks...');
                [player, victim] = this.attack(player, victim);
            }
            if (!skip.victim && victim.alive) {
                console.log('Monster attacks...');
                [victim, player] = this.attack(victim, player);
            }

            if (!player.alive) {
                console.log('Combat: player dead');
                // You dead
                // :(
            }
            if (!victim.alive) {
                console.log('Combat: monster dead');
                // He dead
                player.xp.cur += victim.xp.cur;
            }

            state.player = player as Entity;
            state.victim = victim;
        }
        return state;
    }

    attack(aggressor: Entity, defender: Entity): [Entity, Entity] {
        if (aggressor && defender && aggressor.alive && defender.alive) {
            const toHit = getEntityAttacks(aggressor);
            const defAC = this.getEntityDataPoint(DataPoints.AC, defender).value;
            let newCurHp, result, roll, isHit, damage;

            toHit.forEach((th) => {
                roll = doRoll();
                isHit = ((roll + th) >= defAC);
                console.log(`- Roll => ${roll + th} [${roll}]`);
                if (isHit) {
                    // Do hit things
                    console.log('- It\'s a hit');
                    damage = doRoll({ ...aggressor.damage, finalMod: getMod(aggressor.stats['str']) });
                    console.log(`- Hit for: ${damage}`);

                    newCurHp = Math.max(0, defender.hp.cur - damage);
                    result = cloneEntity(defender);
                    result.hp.cur = newCurHp;
                    defender = result;
                } else {
                    // Do miss things
                    console.log('- It\'s a miss');
                }
            });

            if (defender.hp.cur <= 0) {
                defender.alive = false;
            }
        }
        return [aggressor, defender];
    }
};
