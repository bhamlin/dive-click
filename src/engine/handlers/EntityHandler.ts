import { GameStateInterface } from "../../context/GameStateInterface";
import { cloneEntity, Entity } from "../Entity";
import { ActionHandler, GameEngine } from "../GameEngine";
import { doRoll, getMod, getXpMax, isEmpty } from "../Tools";

export class EntityHandler implements ActionHandler {
    handle(state: GameStateInterface, data?: any): GameStateInterface {
        if ('actionGroup' in data) {
            if (data.actionGroup === 'set-player') {
                return this.setPlayerThing(state, data.action, data.opts);
            }

            console.log(`Unknown action group: ${data.actionGroup}`);
        }

        return state;
    }

    setPlayerThing(state: GameStateInterface, actionTag: string, opts: any) {
        const actions = actionTag.split('-').slice(2);
        const action = actions[0];
        const extra = actions.slice(1);
        let player = cloneEntity(state.player);

        if (action === 'name' && opts && 'name' in opts) {
            player = cloneEntity(player);
            player.name = opts.name;
        } else if (action === 'stat' && opts && 'amount' in opts) {
            player = this.modEntityStat(player, extra[0], opts.amount);
        } else if (action === 'level' && opts && 'amount' in opts) {
            // Level up/down?
            player = this.entityLevelChange(player, opts.amount);
        } else {
            console.log(`Non-handled thing: ${action} (${extra})`, opts);
        }

        this.update(player);
        state.player = player as Entity;
        return state;
    }

    entityLevelChange(entity: Entity, delta: number): Entity {
        const ge = GameEngine.getInstance();
        let roll;

        // Level 1 is the minimum.
        if (entity.level === 1 && delta === -1) {
            return entity;
        }

        // Level
        const old_level = entity.level;
        const new_level = old_level + delta;
        entity.level = new_level;

        // HP
        if (delta > 0) {
            // Up?
            roll = Math.max(1, doRoll({
                sides: entity.hitDie,
                // finalMod: getMod(entity.statBlock.stats['con']),
            }));
            entity.hpRollLog[`level-${new_level}`] = roll;
            entity.hp.max = ge.getEntityHpRolls(entity).reduce((a, e) => (a + e), 0);
            entity.hp.cur = Math.min(entity.hp.cur + roll, entity.hp.max);
        } else if (delta < 0) {
            // Down?
            const slug = `level-${old_level}`;
            if (slug in entity.hpRollLog) {
                roll = entity.hpRollLog[slug] || entity.hitDie;
                entity.hp.max -= roll;
                entity.hp.cur = Math.min(entity.hp.max, entity.hp.cur);
                delete entity.hpRollLog[slug];
            }
        } else {
            // Whut?
        }

        return entity;
    }

    modEntityStat(entity: Entity, stat: string, delta: number): Entity {
        const stats = entity.stats;

        if (stat in stats) {
            const prev = stats[stat];
            stats[stat] = Math.max(3, Math.min(60, prev + delta));
        }

        return entity;
    }

    update(entity: Entity): void {
        const L = entity.hpRollLog;
        const S = entity.stats;

        // HP
        if ((entity.level > 0) && ((entity.hp.max === 0) || isEmpty(entity.hpRollLog))) {
            let roll;
            if (entity.level > 0) {
                roll = entity.hitDie;
                entity.hp.max = roll + Math.max(0, getMod(S['con']));
                L['level-1'] = roll;
            }
            entity.hp.cur = entity.hp.max;
        } else {
            // Make HP max reflect stats
            let hpm = entity.hpRollLog['level-1'] + Math.max(0, getMod(S['con']));
            hpm += Object.values(L).slice(1).map((e: any) => (Math.max(1, e + getMod(S['con'])))).reduce((a, e) => (a + e), 0);
            entity.hp.max = hpm; //Object.values(L).map((e) => (Math.max(1, e + getMod(S['con'])))).reduce((a, e) => (a + e));
            entity.hp.cur = Math.min(entity.hp.max, entity.hp.cur);
        }

        // XP
        if (entity.level > 0) {
            entity.xp.max = getXpMax(entity.level);
        }
    }
}
