import { BaseAttackBonusType, DEFAULT_STATS, getXpMax, Journal, RangeValues, RollParams, StatBlock } from "./Tools";

const DEFAULT_ENTITY = {
    alive: true,
    name: 'Unknown',
    hp: { cur: 8, max: 8 } as RangeValues,
    mp: { cur: 8, max: 8 } as RangeValues,
    hitDie: 8,
    hpRollLog: {
        'level-1': 8,
    },
    level: 1,
    babType: BaseAttackBonusType.Medium,
    damage: { sides: 4, count: 1 },
    stats: DEFAULT_STATS as StatBlock,
    xp: { cur: 0, max: getXpMax(1) } as RangeValues,
    money: 0,
} as Entity;

const copyRangeValues = (source: RangeValues): RangeValues => (
    { cur: source.cur, max: source.max } as RangeValues
);

export type Entity = {
    alive: boolean,
    name: string,
    hp: RangeValues,
    mp: RangeValues,
    hitDie: number,
    hpRollLog: Journal,
    level: number,
    babType: BaseAttackBonusType,
    damage: RollParams,
    stats: StatBlock,
    xp: RangeValues,
    money: number,
};

export const cloneEntity = (source: Entity): Entity => {
    // console.log('enter', 'Entity::cloneEntity', source);
    // console.log('line', 'Entity::cloneEntity', source.hp);
    let out = {
        alive: source.alive,
        name: source.name,
        hitDie: source.hitDie,
        level: source.level,
        babType: source.babType,
        damage: { ...source.damage }
    } as Entity;
    out.hp = { cur: source.hp.cur, max: source.hp.max } as RangeValues;
    out.mp = copyRangeValues(source.mp);
    out.xp = copyRangeValues(source.xp);
    out.hpRollLog = Object.assign({}, ...Object.keys(source.hpRollLog).map((e) => ({ [e]: source.hpRollLog[e] })));
    out.stats = Object.assign({}, ...Object.keys(source.stats).map((e) => ({ [e]: source.stats[e] })));
    // console.log('line', 'Entity::cloneEntity', out.hp);
    // console.log('exit', 'Entity::cloneEntity', out);
    return out;
};

export const getDefaultEntity = (level: number = 1) => {
    const out = cloneEntity(DEFAULT_ENTITY);

    out.xp.cur = 101;

    return out;
};

export const getDefaultPlayer = () => {
    const p = cloneEntity(DEFAULT_ENTITY);

    p.hitDie = 10;
    p.hp.max = p.hp.cur = 10;
    p.hpRollLog['level-1'] = 10;
    p.babType = BaseAttackBonusType.High;
    p.damage.sides = 6

    return p;
};
