import _ from "lodash";
import { Entity } from "./Entity";

/**/

// const NAME_LIST: { [race: string]: string[] } = {};
export const NAME_LIST: Dict<string[]> = {};
fetch(`${process.env.PUBLIC_URL}/content/lists/names.json`)
    .then((r) => r.json())
    .then((data: Dict<string[]>) => (
        Object.keys(data).forEach((key: string) => (
            NAME_LIST[key] = [...data[key]]))));

/**/

export type Dict<V> = {
    [key: string]: V
};

export interface Clonable<T> {
    clone(): T;
};

export type RangeValues = {
    cur: number,
    max: number,
};

export type RollParams = Partial<{
    sides: number,
    qty: number,
    perDieMod: number,
    finalMod: number,
}>;

export type StatBlock = { [name: string]: number; }

export type Journal = Dict<number>;

export enum DataPoints {
    Nothing = 1,
    HP,
    AC,
    AttackBonus,
};

export enum BaseSaveBonusType {
    High = 1,
    Low,
};

export enum BaseAttackBonusType {
    High = 1,
    Medium = (3 / 4),
    Low = (1 / 2),
};

/**/

export const default_to = <T>(value: T | undefined, default_value: T): T => (
    (value === undefined) ? (default_value) : (value)
);

export const doRoll = (params?: RollParams): number => {
    const P = Object.assign({
        sides: 20, qty: 1, perDieMod: 0, finalMod: 0,
    }, params);
    const roll = () => (Math.floor((Math.random() * P.sides) + 1 + P.perDieMod));
    let result = 0;

    for (let i = 0; i < P.qty; i++) {
        result += roll();
    }

    return result + P.finalMod;
};

export const getAttacks = (level: number, babType: BaseAttackBonusType = BaseAttackBonusType.Medium, perMod: number = 0): number[] => {
    const result: number[] = [];
    let bab = getBaB(level, babType);

    result.push(bab + perMod);
    while (bab > 5) {
        bab -= 5;
        result.push(bab + perMod);
    }

    return result;
};

// export const getAttacksString = (level: number, babType: BaseAttackBonusType = BaseAttackBonusType.Medium, perMod: number = 0): string => {
//     const attacks = getAttacks(level, babType, perMod);
//     return `${(attacks.map((e) => (e)).join(' / '))}`;
// };

export const getBaB = (level: number, babType: BaseAttackBonusType = BaseAttackBonusType.Medium): number => {
    return Math.floor(level * babType);
};

export const getEntityAttacks = (entity: Entity) => (
    getAttacks(entity.level, entity.babType, getMod(entity.stats['str']))
)

export const getMod = (value: number): number => {
    return Math.floor(value / 2) - 5;
};

export const getRandomName = (race: string = 'human'): string => (
    _.sample(NAME_LIST[race]) as string
);

export const getXpMax = (level: number): number => {
    return Math.floor(((level + 1) * level) / 2) * 1000;
};

export const isEmpty = (dict: Dict<any>): boolean => (
    Object.keys(dict).length < 1
);

/**/

export const DEFAULT_STATS: { [key: string]: number } = {
    "str": 8,
    "dex": 8,
    "con": 8,
    "int": 8,
    "wis": 8,
    "cha": 8,
};

export const STAT_METADATA: { [key: string]: { name: string } } = {
    "str": { "name": "Strength" },
    "dex": { "name": "Dexterity" },
    "con": { "name": "Constitution" },
    "int": { "name": "Intelligence" },
    "wis": { "name": "Wisdom" },
    "cha": { "name": "Charisma" }
};

/**/

// export class Tools {
//     private static nameList = [
//         "Davebear", "Owltim", "Boblin", "Rogorc"
//     ];

//     static base_stats: { [key: string]: { name: string } } = {
//         "str": { "name": "Strength" },
//         "dex": { "name": "Dexterity" },
//         "con": { "name": "Constitution" },
//         "int": { "name": "Intelligence" },
//         "wis": { "name": "Wisdom" },
//         "cha": { "name": "Charisma" }
//     };

//     static randomName(): string {
//         return this.nameList[Math.floor(Math.random() * this.nameList.length)]
//     }

//     static randomHp(base: number, variability: number): number {
//         return Math.floor((Math.random() * variability) + 1 + base);
//     }

//     static randomEntity(): Entity {
//         const hpVal = this.randomHp(10, 5);
//         return {
//             name: this.randomName(),
//             hp: { cur: hpVal, max: hpVal },
//         } as Entity;
//     }
// };
