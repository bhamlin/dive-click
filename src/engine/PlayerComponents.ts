import { EntityComponent, EntityClassComponent } from "./Components";

export interface PlayerComponent extends EntityComponent {

}

export interface PlayerClassComponent extends PlayerComponent, EntityClassComponent {

}

export class Fighter implements PlayerComponent {
    name = 'player-class-fighter';

    getTriggers() {
        return ['player-level-change']
    }
}