import { createContext, Dispatch, ReactNode, SetStateAction, useContext, useState } from "react";

import { GameEngine } from "../engine/GameEngine";
import { GameState, GameStateInterface } from "./GameStateInterface";

const GameStateContext = createContext({
    state: {} as GameState,
    setState: {} as Dispatch<SetStateAction<GameState>>,
});

export const GameStateProvider = ({
    children,
    value = GameEngine.getDefaultState() as GameStateInterface,
}: {
    children: ReactNode,
    value?: GameState
}) => {
    const [state, setState] = useState(value);
    return (
        <GameStateContext.Provider value={{ state, setState }}>
            {children}
        </GameStateContext.Provider>
    )
};

export const useGlobalState = () => {
    const context = useContext(GameStateContext);
    if (!context) {
        throw new Error("useGlobalState must be used within a GlobalStateContext");
    }
    return context;
};
