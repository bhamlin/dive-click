import { Entity, getDefaultPlayer } from "../engine/Entity";
import { Dict } from "../engine/Tools";

export interface GameStateInterface {
    player: Entity;
    victim: Entity | undefined;
    alive: boolean;
    status: {
        inError: boolean,
        msgError?: string,
    },
    flags: Dict<string | number>;
};

export const DEFAULT_STATE = {
    player: getDefaultPlayer(),
    victim: undefined,
    alive: false,
    status: {
        inError: false,
        msgError: undefined,
    },
    flags: {},
}

export interface GameState extends Partial<GameStateInterface> { }
