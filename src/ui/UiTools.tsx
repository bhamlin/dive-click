import { ReactNode } from "react";
import { Col, Row } from "react-bootstrap"

import { Journal } from "../engine/Tools";

type WithChildren = {
    children: ReactNode,
};

type WithChildrenValue = WithChildren & {
    value: number,
};

type WithChildrenTitle = WithChildren & {
    title: string,
};

export const ExplainNoneView = (props: WithChildrenValue) => (
    <RC>
        {props.children} {props.value}
    </RC>
);

export const ExplainLessView = (props: WithChildrenValue & { entries: any[] }) => (
    <RC>
        {props.children} {props.value} ({props.entries.map((e) => (
            e
        )).join(', ')})
    </RC>
);

export const ExplainView = (props: WithChildrenValue & { journal: Journal }) => (
    <RC>
        {props.children} {props.value} ({Object.keys(props.journal).map((e) => (
            `${e} = ${props.journal[e]}`
        )).join(', ')})
    </RC>
);

export const ExplainStat = ({ statName, statValue, statMod }: { statName: string, statValue: number, statMod: number }) => (
    <div className="to-center">
        <div className="stat-name">{statName}</div>
        <div>{statValue} ({statMod >= 0 ? '+' : ''}{statMod})</div>
    </div>
);

export const HeaderSection = (props: WithChildrenTitle) => (
    <>
        <RC colClassName="to-center">&laquo; {props.title} &raquo;</RC>
        {props.children}
    </>
);

export const NameSection = ({ children }: WithChildren) => (
    <Row className="player-name to-center">
        <Col className="d-flex align-items-start justify-content-center">&#12300;</Col>
        <Col>{children}</Col>
        <Col className="d-flex align-items-end justify-content-center">&#12301;</Col>
    </Row>
);

export const OptionalSection = (props: WithChildren & { isVisible?: boolean }) => (
    <>
        {props.isVisible === true &&
            <>
                {props.children}
            </>
        }
    </>
);

export const OptionalHeaderSection = (props: WithChildrenTitle & { isVisible?: boolean }) => (
    <OptionalSection isVisible={props.isVisible}>
        <HeaderSection title={props.title}>
            {props.children}
        </HeaderSection>
    </OptionalSection>
);

export const RC = (props: { children: any, rowClassName?: string, colClassName?: string }) => (
    <Row className={props.rowClassName}>
        <Col className={props.colClassName}>
            {props.children}
        </Col>
    </Row >
);

export const RCHR = ({ hrClassName }: { hrClassName?: string }) => (
    <RC>
        <hr className={hrClassName} />
    </RC>
);

export const ShowView = (props: { children: ReactNode, content: any }) => (
    <RC>
        {props.children} {props.content}
    </RC>
);
