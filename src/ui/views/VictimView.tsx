import { useGlobalState } from "../../context/GameContext";

const VictimView = () => {
    const { state } = useGlobalState();

    if (state.victim === undefined) {
        return (
            <div><span>Waiting for new victim...</span></div>
        );
    } else {
        return (
            <div>
                <span>
                    {state.victim.name}: [{state.victim.hp.cur}/{state.victim.hp.max}]
                </span>
            </div>
        );
    }
};

export default VictimView;