import { Button, Col, Container, Form, Row } from "react-bootstrap";
import { Dice3Fill } from "react-bootstrap-icons";

import { useGlobalState } from "../../context/GameContext";
import { Entity } from "../../engine/Entity";
import { GameEngine } from "../../engine/GameEngine";
import { DataPoints, getEntityAttacks, getMod, getRandomName } from "../../engine/Tools";
import { ExplainLessView, ExplainStat, ExplainView, RC, ShowView } from "../UiTools";

const PlayerStatBlockView = (props: any) => {
    // eslint-disable-next-line
    const { state, setState } = useGlobalState();
    // eslint-disable-next-line
    const ge = GameEngine.getInstance();

    const p = state.player as Entity;

    const ac_data = ge.getEntityDataPoint(DataPoints.AC, state.player);
    const hp_data = ge.getEntityDataPoint(DataPoints.HP, state.player);
    const hp_rolls = ge.getEntityHpRolls(p);

    const setPlayerName = (value: string) => (
        setState({ ...ge.action(state, 'set-player-name', { name: value }) })
    );

    return (
        <Container className="stat-block border">
            <RC>
                <Form.Group className="d-flex" controlId="sb-entity-name">
                    <Form.Control className="entity-name" type="input"
                        value={p.name} placeholder="Player Name" onChange={
                            // (e) => (setState({ ...ge.action(state, 'set-player-name', { name: e.target.value }) }))
                            (e) => (setPlayerName(e.target.value))
                        } />
                    <span>&nbsp;</span>
                    <Button><Dice3Fill onClick={
                        // (e) => (setState({ ...ge.action(state, 'set-player-name', { name: getRandomName() }) }))
                        () => (setPlayerName(getRandomName()))
                    } /></Button>
                </Form.Group>
            </RC>
            <RC colClassName="entity-desc">Human</RC>
            <hr />
            <ExplainView value={ac_data.value} journal={ac_data.journal}>
                Armor Class
            </ExplainView>
            <ExplainLessView value={hp_data.value} entries={hp_rolls}>
                HP
            </ExplainLessView>
            {/* <ShowView content={getAttacksString(p.level, BaseAttackBonusType.High, getMod(p.stats['str']))}>
                Attack Bonus
            </ShowView> */}
            <ShowView content={`${(getEntityAttacks(p).map((e) => (e)).join(' / '))}`}>
                Attack Bonus
            </ShowView>
            <hr />
            <Row>
                {Object.keys(p.stats).map((stat) => (
                    <Col key={`col-stat-explain-${stat}`} className="d-flex justify-content-center">
                        <ExplainStat key={`stat-explain-${stat}`} statName={stat.toUpperCase()} statValue={p.stats[stat]} statMod={getMod(p.stats[stat])} />
                    </Col>
                ))}
            </Row>
            {/* <hr /> */}
        </Container>
    );
};

export default PlayerStatBlockView;