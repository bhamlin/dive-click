import { Col, Container, Row, Button } from "react-bootstrap";

import { useGlobalState } from "../../context/GameContext";
import { GameStateInterface } from "../../context/GameStateInterface";
import { GameEngine } from "../../engine/GameEngine";
import { DataPoints, default_to, getEntityAttacks, getMod, STAT_METADATA } from "../../engine/Tools";
import { ActionButton } from "../components/ActionButton";
import { HeaderSection, NameSection, OptionalHeaderSection } from "../UiTools";

/* Types */

type Editable = Partial<{
    editable: boolean,
    modButtons: any,
}>

type StandardOpts = {
    statName: string,
}

type StandardOptsValue = StandardOpts & {
    statValue: any,
}

type PlayerViewOpts = Partial<{
    editable: boolean,
    actionsVisible: boolean,
    miscVisible: boolean,
}>

/* React */

const StatLine = (props: Editable & StandardOptsValue) => {
    const { state, setState } = useGlobalState();
    const ge = GameEngine.getInstance();

    let editCol = (<></>);
    if (props.editable && props.modButtons) {
        const m = props.modButtons;

        editCol = (
            <>
                {Object.keys(m).map((e: string) => (
                    <Button key={m[e].action} size={'sm'} onClick={() => setState({
                        ...ge.action(state, m[e].action, m[e].opts)
                    })}>{m[e].label}</Button>
                ))}
            </>
        );
    }
    return (
        <Row>
            <Col xs={4}>{props.statName}:</Col>
            <Col className="to-right">
                {editCol}
            </Col>
            <Col xs={4} className="to-right">{props.statValue}</Col>
        </Row>
    );
}

const StatWithMod = (props: Editable & StandardOptsValue) => (
    <StatLine statName={props.statName} statValue={`${props.statValue} [${getMod(props.statValue)}]`}
        editable={props.editable} modButtons={props.modButtons} />
);

const StatResource = (props: Editable & StandardOpts & { statCur: any, statMax: any }) => (
    <StatLine statName={props.statName} statValue={`${props.statCur}/${props.statMax}`}
        editable={props.editable} modButtons={props.modButtons} />
);

const StatRangeResource = (props: Editable & StandardOpts & { stat: { cur: any, max: any } }) => (
    <StatResource statName={props.statName} statCur={props.stat.cur} statMax={props.stat.max}
        editable={props.editable} modButtons={props.modButtons} />
);

/* Functions / Tools */

const statLevelFunc = () => ({
    plus: { label: '+', action: `set-player-level-up`, opts: { amount: 1 } },
    minus: { label: '-', action: `set-player-level-down`, opts: { amount: -1 } },
});

const statModFunc = (e: string) => ({
    plus: { label: '+', action: `set-player-stat-${e}-up`, opts: { amount: 1 } },
    minus: { label: '-', action: `set-player-stat-${e}-down`, opts: { amount: -1 } },
});

/* Main widget */

const PlayerView = (props: PlayerViewOpts) => {
    const { state, setState } = useGlobalState();
    const ge = GameEngine.getInstance();
    const cstate = ge.getCompleteState(state) as GameStateInterface;

    const p = cstate.player;
    const s = p.stats;


    const clickHandler = (action: string, opts?: any): void => (
        setState({ ...ge.action(state, action, opts) })
    );

    return (
        <Container className="border monospaced" fluid>
            <NameSection>
                {p.name}
            </NameSection>

            <hr />

            <HeaderSection title="Core">
                {Object.keys(p.stats).map((e) => (
                    <StatWithMod key={`stat-${e}`} statName={STAT_METADATA[e].name} statValue={s[e]}
                        editable={default_to(props.editable, false)} modButtons={statModFunc(e)} />
                ))}
            </HeaderSection>

            <HeaderSection title="Status">
                <StatRangeResource statName='HP' stat={p.hp} />
                {/* <StatRangeResource statName='MP' stat={p.mp} /> */}
                <StatLine statName="Armor&nbsp;Class" statValue={
                    ge.getEntityDataPoint(DataPoints.AC, state.player).value} />
                {/* <StatLine statName="Attack&nbsp;Bonus" statValue={
                    ge.getEntityDataPoint(DataPoints.AttackBonus, state.player).value} /> */}
                <StatLine statName="Attack&nbsp;Bonus" statValue={
                    `${(getEntityAttacks(p).map((e) => (e)).join('/'))}`} />
            </HeaderSection>

            <HeaderSection title="Progress">
                <StatLine statName='Level' statValue={p.level} editable={default_to(props.editable, false)} modButtons={statLevelFunc()} />
                <StatRangeResource statName='XP' stat={p.xp} />
            </HeaderSection>

            <OptionalHeaderSection title="Misc" isVisible={default_to(props.miscVisible, true)}>
                <div />
            </OptionalHeaderSection>

            <OptionalHeaderSection title="Actions" isVisible={default_to(props.actionsVisible, true)}>
                <Row className='d-flex'>
                    <Col className='d-flex'><ActionButton clickHandler={clickHandler} action='attack' >Attack!</ActionButton></Col>
                    <Col className='d-flex'><ActionButton clickHandler={clickHandler} action='flee' variant="warning">Flee!</ActionButton></Col>
                    <Col className='d-flex'><ActionButton clickHandler={clickHandler} action='heal' variant="success">Heal!</ActionButton></Col>
                </Row>
            </OptionalHeaderSection>

            <br />
        </Container>
    );
}

export default PlayerView;