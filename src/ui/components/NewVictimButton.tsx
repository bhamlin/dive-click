import { useGlobalState } from "../../context/GameContext";
import { GameEngine } from "../../engine/GameEngine";

export const NewVictimButton = () => {
    const { state, setState } = useGlobalState();
    const ge = GameEngine.getInstance();

    return (
        <button type="button" onClick={() => setState({ ...state, victim: ge.getRandomEntity() })}>New Victim</button>
    );
};
