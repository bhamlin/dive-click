import { ReactNode } from "react";
import { Button } from "react-bootstrap";

type ActionInput = {
    action: string,
    children: ReactNode,
    clickHandler: (actionString: string) => void,  //state: SetStateAction<GameState>
};

export const ActionButton = ({ action, children, clickHandler, variant }: ActionInput & { variant?: string }) => {
    if (!variant) { variant = 'primary' }
    return (
        <Button type="button" variant={variant} onClick={() => clickHandler(action)}>
            {children}
        </Button>
    );
};