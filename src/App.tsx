import { Button, Col, Container, Row } from "react-bootstrap";
import { Link, Outlet } from "react-router-dom";

import { useGlobalState } from "./context/GameContext";
import { RC, RCHR } from "./ui/UiTools";

import "./css/App.css";
import { GameState } from "./context/GameStateInterface";

const gameStateKey = 'dive-click-state';

const SaveText = 'Save';
const LoadText = 'Load';
const ClearStateText = 'Delete';

const clearSavedState = (): void => (
    localStorage.removeItem(gameStateKey)
)

export const App = () => {
    const { state, setState } = useGlobalState();
    let createLink, playLink, welcomeLink, doLoadLink;

    const onSaveOrLoad = (e: any) => {
        const verb = e.target.textContent;
        switch (verb) {
            case SaveText:
                localStorage.setItem(gameStateKey, JSON.stringify(state));
                break;
            case LoadText:
                const savedState = localStorage.getItem(gameStateKey);
                if (savedState) {
                    setState(JSON.parse(savedState) as GameState);
                }
                break;
            case ClearStateText:
                clearSavedState();
                setState({ ...state, alive: false });
                break;
            default:
                console.log('do what?');
        }
    }

    if (state.alive) {
        createLink = (<Link to="/create">Edit!</Link>);
        playLink = (<Link to="/play">Play!</Link>);
    } else {
        createLink = (<Link to="/create" onClick={() => setState({ ...state, alive: true })}>Create!</Link>);
        playLink = (<></>);
    }

    welcomeLink = (<Link to="/">Welcome!</Link>);

    return (
        <div className="App">
            <Container fluid>
                <Row>
                    <Col>{createLink}</Col>
                    <Col>{playLink}</Col>
                    <Col>
                        <Button size={"sm"} variant="light" onClick={onSaveOrLoad}>{SaveText}</Button>
                        <span> | </span>
                        <Button size={"sm"} variant="warning" onClick={onSaveOrLoad}>{LoadText}</Button>
                        <span> | </span>
                        <Button size={"sm"} variant="danger" onClick={onSaveOrLoad}>{ClearStateText}</Button>
                    </Col>
                    <Col className="to-right">{welcomeLink}</Col>
                </Row>

                <RCHR />

                <RC>
                    <Outlet />
                </RC>

                <RCHR />
            </Container>
        </div>
    );
};