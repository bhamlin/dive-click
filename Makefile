build: build/index.html build.tar.xz

build.tar.xz:
	cd build
	tar cvJf build.tar.xz -C build/ .
	cd ..

build/index.html:
	npm run build-gcs

clean:
	rm -rfv build
	rm -rfv build.tar.xz
